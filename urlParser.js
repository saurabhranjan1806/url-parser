/**
 * @author Saurabh Ranjan
 * importing modules
 */
const Promise = require('bluebird');
const readline = require('readline');

/**
 * Initial configuraitons
 */
let globalUrl;
const reader = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
/**
 * this function asks question for you
 * @param {string} question - question to print in console
 */
const getInput = (question) =>
  new Promise((resolve, reject) => {
    reader.question(question, (answer) => {
      resolve(answer);
    });
  });

/**
 * this function takes the input from the console for the url
 */
const getUrl = () =>
  getInput('Enter the url: ').then((answer) => {
    if (answer.length > 0) {
      return answer;
    }
    console.log('Please enter the correct url');
    return getUrl();
  });

/**
 * this function extracts the scheme part of the url
 */
const getScheme = () => {
  const charToLook = '//';
  let scheme = null;
  if (globalUrl.indexOf(charToLook) > -1) {
    scheme = globalUrl.split(':')[0];
    if (scheme.length > 0) {
      globalUrl = globalUrl.substring(
        globalUrl.indexOf(':') + 1,
        globalUrl.length
      );
    }
  }
  return scheme;
};

/**
 * this function extracts the username and password part of the url
 */
const getUsernameAndPassword = () => {
  const charToLook = '@';
  let username = null;
  let password = null;
  if (globalUrl.indexOf(charToLook) > -1) {
    if (globalUrl.indexOf('//') > -1) {
      globalUrl = globalUrl.substring(
        globalUrl.indexOf('//') + 2,
        globalUrl.length
      );
    }
    const splitArray = globalUrl.split('@');
    const credString = splitArray[0];
    if (credString.indexOf(':') > -1) {
      const credArray = credString.split(':');
      username = credArray[0];
      password = credArray[1];
    } else {
      username = credString;
    }

    // removing from globalurl
    globalUrl = globalUrl.substring(
      globalUrl.indexOf(charToLook),
      globalUrl.length
    );
  }
  return { username, password };
};

/**
 * @param {string} str - returns the eligible domain or port
 */
const getEligiblePortOrDomian = (str) => {
  if (str.indexOf('/') > -1) {
    return str.split('/')[0];
  }
  if (str.indexOf('?') > -1) {
    return str.split('?')[0];
  }
  if (str.indexOf('#') > -1) {
    return str.split('#')[0];
  }
};

/**
 * this function extracts the domain and port part of the url
 */
const getDomainAndPort = () => {
  const charToLook = '@';
  let domain = null;
  let port = null;

  let splitArray;
  let domainString;
  if (globalUrl.indexOf(charToLook) > -1) {
    splitArray = globalUrl.split('@');
    domainString = splitArray[1];
  } else if (globalUrl.indexOf('//') > -1) {
    globalUrl = globalUrl.substring(
      globalUrl.indexOf('//') + 2,
      globalUrl.length
    );
    domainString = globalUrl;
  }

  if (domainString.indexOf(':') > -1) {
    const domainArray = domainString.split(':');
    domain = domainArray[0];
    const portCandidate = domainArray[1];
    port = getEligiblePortOrDomian(portCandidate);
  } else {
    domain = getEligiblePortOrDomian(domainString);
  }

  // removing from globalurl
  if (port) {
    globalUrl = globalUrl.substring(
      globalUrl.indexOf(port) + port.length,
      globalUrl.length
    );
  } else if (domain) {
    globalUrl = globalUrl.substring(
      globalUrl.indexOf(domain) + domain.length,
      globalUrl.length
    );
  }
  return { domain, port };
};

/**
 * this function extracts the fragment part of the url
 */
const getFragment = () => {
  const charToLook = '#';
  let fragment = null;
  if (globalUrl.indexOf(charToLook) > -1) {
    fragment = globalUrl.split(charToLook)[1];
    if (fragment.length > 0) {
      globalUrl = globalUrl.substring(0, globalUrl.indexOf(charToLook));
    }
  }
  return fragment;
};

/**
 * this function extracts the query part of the url
 */
const getQuery = () => {
  const charToLook = '?';
  let query = null;
  if (globalUrl.indexOf(charToLook) > -1) {
    query = globalUrl.split(charToLook)[1];
    if (query.length > 0) {
      globalUrl = globalUrl.substring(0, globalUrl.indexOf(charToLook));
    }
  }
  return query;
};

/**
 * this function parse the url and returns the parts
 * @param {string} url - url to be parsed
 */
const parseUrl = (url) => {
  let scheme;
  let domain;
  let port;
  let path;
  let query;
  let fragment;
  let username;
  let password;
  globalUrl = url;
  // console.log({ globalUrl });
  // extract scheme
  scheme = getScheme();

  // extract username and password
  let res = getUsernameAndPassword();
  username = res.username;
  password = res.password;

  // extract domain and port
  res = getDomainAndPort();
  domain = res.domain;
  port = res.port;

  // extract fragment
  fragment = getFragment();

  // extract query
  query = getQuery();

  // extract path
  path = globalUrl;

  return {
    scheme,
    domain,
    port,
    path,
    query,
    fragment,
    username,
    password,
    // globalUrl,
  };
};

/**
 * this is the starting point of the application
 */
const main = () =>
  getUrl()
    .then((url) => parseUrl(url))
    .then((parts) => {
      console.log(parts);
      return Promise.resolve();
    })
    .finally(() => {
      reader.close();
    })
    .catch((err) => {
      console.log(
        "Ohhh! Please report the error to the developer if you didn't intend it to end.",
        { err }
      );
      return Promise.reject(err);
    });

main();
